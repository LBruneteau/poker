from module_poker import jeuCartes , pioche , comparMain , msgMain
import time

#CETTE VERSION N'EST QUE SECONDAIRE DONC PAS MAL BUGGÉE ET CODÉE N'IMPORTE COMMENT !!!!!
#CE CODE VA VOUS DÉCHIRER LES YEUX !

class Joueur:
    '''
    Classe des joueurs de poker
    Contient toutes les fonctions nécéssaires à une partie
    gere les joueurs mais pas les cartes
    '''

    nb_joueurs:int = 0
    nb_joueurs_temp:int = 0 #nb de joueurs au début d'un tour
    nb_joueurs_restants:int = 0 #ceux qui ne sont pas couchés
    liste_joueurs:list = [] 
    liste_joueurs_temp:list = [] #meme chose mais c'est une liste de Joueur
    liste_joueurs_restants:list = []
    pot:int = 0 #argent misé dans une partie
    jeu:list = jeuCartes()
    joueur_actuel:int = 0 #index dans la liste des joueurs 
    mise_max:int = 0
    aligne:bool = False
    milieu:list = [] #cartes communes

    def __init__(self,nom:str):
        '''
        creation d'un objet de type joueur
        '''
        self.nom:str = nom
        self.argent:str = 1500
        self.mise:int = 0
        self.cartes:list = []
        self.a_joue:bool = False
        self.est_tapis:bool = False
        self.est_dealer:bool = False
        self.est_petite_blinde:bool = False
        self.est_grosse_blinde:bool = False
        self.est_utg:bool = False
        self.combi:list = []
        Joueur.nb_joueurs += 1
        Joueur.liste_joueurs.append(self)

    def __repr__(self):
        '''
        permet d'afficher un joueur. 
        D'abord les rôles (D , SB , BB , UTG)
        Puis le nom, l'argent et la mise
        '''
        msg = ''
        if self.est_dealer:
            msg += 'D '
        if self.est_petite_blinde:
            msg += 'SB '
        if self.est_grosse_blinde:
            msg += 'BB '
        if self.est_utg:
            msg += 'UTG '
        msg += ' ' * (8 - len(msg))
        msg += self.nom.center(20 , ' ')
        msg += ' [{}] : {}'.format(str(self.argent).center(6,' ') , str(self.mise).center(5 , ' '))
        if Joueur.liste_joueurs_restants[Joueur.joueur_actuel] is self:
            msg += " <- "
        return msg

    def affJoueurs(cls):
        '''
        afficher tous les joueurs
        '''
        for j in cls.liste_joueurs:
            print(j)
    affJoueurs = classmethod(affJoueurs)

    def affJoueursRestants(cls):
        '''
        afficher les joueurs restants
        '''
        for j in cls.liste_joueurs_restants:
            print(j)
    affJoueursRestants = classmethod(affJoueursRestants)

    def initJeu(cls):
        '''
        Creation d'une partie
        Enregistrement des joueurs, initialisation des variables etc...
        '''
        try:
            cb_joueurs:int = int(input('Combien de joueurs ? : '))
            assert cb_joueurs > 1 #2 joueurs min 23 max
            assert cb_joueurs < 24
            for j in range(cb_joueurs):
                nom:str = input('Nom du joueur {} : '.format(j + 1))
                Joueur(nom)
            cls.nb_joueurs_restants = cls.nb_joueurs
            cls.liste_joueurs_restants = cls.liste_joueurs[:]
            cls.nb_joueurs_temp = cls.nb_joueurs
            #Attribution des rôles
            cls.liste_joueurs_temp = cls.liste_joueurs[:]
            cls.liste_joueurs[0].est_dealer = True
            cls.liste_joueurs[1].est_petite_blinde = True
            if cls.nb_joueurs == 2:
                cls.liste_joueurs[0].est_grosse_blinde = True
                cls.liste_joueurs[1].est_utg = True
            elif cls.nb_joueurs == 3:
                cls.liste_joueurs[2].est_grosse_blinde = True
                cls.liste_joueurs[0].est_utg = True
            else:
                cls.liste_joueurs[2].est_grosse_blinde = True
                cls.liste_joueurs[3].est_utg = True
        except ValueError:
            print("Nombre invalide !")
            cls.initJeu()
        except AssertionError:
            print("Le nombre de joueur doit être compris entre 2 et 23 (inclus)")
            cls.initJeu()
    initJeu = classmethod(initJeu)

    def initPot(cls):
        '''
        permet de démarrer un pot.
        remélange les cartes, fait revenir les joueurs couchés...
        '''
        #reset des variables
        cls.nb_joueurs_restants = cls.nb_joueurs
        cls.liste_joueurs_restants = cls.liste_joueurs[:]
        cls.jeu = jeuCartes()
        cls.pot = 0
        cls.joueur_actuel = 0
        cls.aligne = False
        cls.mise_max = 0
        cls.milieu =[]
        cls.nb_joueurs_temp = cls.nb_joueurs
        cls.liste_joueurs_temp = cls.liste_joueurs[:]
        #reset des attributs
        for j in cls.liste_joueurs:
            j.mise = 0
            j.cartes = []
            j.a_joue = False
            j.est_tapis = False
            i = cls.liste_joueurs.index(j)
            #tourner les blindes
            if j.est_dealer:
                index_d = i
                j.est_dealer = False
            if j.est_petite_blinde:
                index_sb = i
                j.est_petite_blinde = False
            if j.est_grosse_blinde:
                index_bb = i
                j.est_grosse_blinde = False
            if j.est_utg:
                index_utg = i
                j.est_utg = False
        if index_d == cls.nb_joueurs - 1:
            cls.liste_joueurs[0].est_dealer = True
        else:
            cls.liste_joueurs[index_d + 1].est_dealer = True
        if index_sb == cls.nb_joueurs - 1:
            cls.liste_joueurs[0].est_petite_blinde = True
        else:
            cls.liste_joueurs[index_sb + 1].est_petite_blinde = True
        if index_bb == cls.nb_joueurs - 1:
            cls.liste_joueurs[0].est_grosse_blinde = True
        else:
            cls.liste_joueurs[index_bb + 1].est_grosse_blinde = True
        if index_utg == cls.nb_joueurs - 1:
            cls.liste_joueurs[0].est_utg = True
        else:
            cls.liste_joueurs[index_utg + 1].est_utg = True
    initPot = classmethod(initPot)

    def distrib(cls):
        '''
        donne 2 cartes à chaque joueur
        '''
        print('Distribution des cartes')
        for j in cls.liste_joueurs:
            cls.jeu , cartes = pioche(cls.jeu , 2)
            j.cartes = cartes
    distrib = classmethod(distrib)

    def fold(self):
        '''
        permet aux joueurs de se coucher
        '''
        print('\n' * 50) #Récurrent dans le programme. permet d'empecher un joueur de voir les cartes de ses adversaires en les faisant sortir de l'ecran
        print('{} se couche'.format(self.nom))
        Joueur.liste_joueurs_restants.remove(self)
        Joueur.nb_joueurs_restants -= 1
        self.a_joue = True

    def check(self):
        '''
        permet de checker
        '''
        print("\n" * 50)
        print('{} check'.format(self.nom))
        self.a_joue = True
        Joueur.joueur_actuel += 1 #On avance le curseur pour passer au joueur suivant

    def bet(self , mise:int):
        '''
        Permet de miser une certaine somme. 
        '''
        if mise >= self.argent:
            print('\n' * 50)
            print('{} mise {} et est à tapis !'.format(self.nom , self.argent))
            self.mise += self.argent
            Joueur.pot += self.argent
            self.argent = 0 
            self.est_tapis = True
            Joueur.mise_max = max(Joueur.mise_max , self.mise)
        else:
            self.mise += mise
            Joueur.pot += mise
            self.argent -= mise
            if self.mise == Joueur.mise_max:
                print('\n' * 50)
                print('{} suit {}'.format(self.nom , mise))
            else:
                print('\n' * 50)
                Joueur.mise_max = self.mise
                print('{} relance a {}'.format(self.nom , Joueur.mise_max))
        Joueur.joueur_actuel += 1
        self.a_joue = True

    def miser(self):
        '''
        gere le tour d'un joueur
        '''
        a_suivre:int = Joueur.mise_max - self.mise
        mise:str = input('Combien misez-vous ? ({} pour suivre) : '.format(a_suivre))
        if not (mise.isnumeric()) or mise == '':
            mise:int = 0 #0 si on met n'importe quoi
        else:
            mise:int = abs(int(mise)) #prendre la valeur absolue evite les négatifs
        if mise < a_suivre:
            mise = 0 #Si on mise moins que pour suivre on se couche
        if mise == 0:
            if a_suivre > 0:
                self.fold()
            else:
                self.check() #Si on mise 0 mais qu'on est aligné on check
        else:
            self.bet(mise)

    def tousAlignes(cls):
        '''
        Verifie si tous les joueurs sont alignés pour aller au tour suivant
        '''
        #Ils sont alignés s'il n'y a qu'un joueur
        if cls.nb_joueurs_restants == 1:
            cls.aligne = True
        else:
            tous_joue = True
            j = 0
            #On regarde s'ils on tous joués
            while tous_joue and j < cls.nb_joueurs_temp:
                tous_joue = cls.liste_joueurs_temp[j].a_joue
                j += 1
            cls.aligne = tous_joue
            j = 0
            #on regarde s'ils sont tous alignés
            while cls.aligne and j < cls.nb_joueurs_restants:
                cls.aligne = cls.liste_joueurs_restants[j].mise == cls.mise_max
                j += 1
    tousAlignes = classmethod(tousAlignes)

    def sousous(cls):
        '''
        retire les joueurs qui n'ont plus d'argent de la partie
        '''
        for j in cls.liste_joueurs:
            if j.argent == 0:
                cls.liste_joueurs.remove(j)
                cls.nb_joueurs -= 1
    sousous = classmethod(sousous)

    def placerBlindes(cls):
        '''
        la petite blinde mise 10 la grosse 20
        '''
        #2 boucles pour que la petite soit placée avant la grosse
        for j in cls.liste_joueurs:
            if j.est_petite_blinde:
                print('{} place la petite blinde'.format(j.nom))
                j.bet(10)
                j.a_joue = False
        for j in cls.liste_joueurs:
            if j.est_grosse_blinde:
                print('{} place la grosse blinde'.format(j.nom))
                j.bet(20)
                j.a_joue = False
    placerBlindes = classmethod(placerBlindes)

    def ajouterCartes(cls,nb_cartes=1):
        '''
        ajoute des cartes au milieu
        utilisé entre les tours
        '''
        cls.jeu , m = pioche(cls.jeu , nb_cartes)
        cls.milieu += m
    ajouterCartes = classmethod(ajouterCartes)

    def combiMain(self):
        '''
        Donne la meilleure combinaison d'un joueur parmi le milieu et les cartes privées
        '''
        from itertools import combinations
        cartes_dispo = Joueur.milieu[:] + self.cartes[:]
        combis = tuple(combinations(cartes_dispo , 5)) #Une combi de poker c'est 5 cartes
        #obtenir le max
        maxi = combis[0]
        for c in combis:
            if comparMain(c , maxi) == 1:
                maxi = c
        self.combi = maxi

    def gagner(self):
        '''
        donne l'argent d'un pot à un joueur
        '''
        print('{} remporte un pot de {}'.format(self.nom , Joueur.pot))
        self.argent += Joueur.pot
        Joueur.pot = 0
        
    def quiCommence(cls):
        '''
        trouve qui commence un tour
        d'abord la SB, puis les autres dans l'ordre
        '''
        #savoir si la SB est encore là
        for j in cls.liste_joueurs:
            if j.est_petite_blinde:
                i = cls.liste_joueurs.index(j)  
        petite_blinde_restante = cls.liste_joueurs[i] in cls.liste_joueurs_restants
        #sinon obtenir le suivant
        if not petite_blinde_restante:
            k = 0
            trouve = False
            while not trouve:
                if i + k >= cls.nb_joueurs_restants:
                    k -= cls.nb_joueurs_restants
                if cls.liste_joueurs[i + k] in cls.liste_joueurs_restants:
                    trouve = True
                    cls.joueur_actuel = cls.liste_joueurs_restants.index(cls.liste_joueurs[i + k])
                k += 1
        else:
            cls.joueur_actuel = i
    quiCommence = classmethod(quiCommence)

    def tour(cls , pre_flop=False):
        '''
        Un tour d'enchere 
        fait jouer tous les joueurs jusqu'a l'alignement
        '''
        while cls.nb_joueurs_restants > 1 and not cls.aligne:
            print("\n")
            j = cls.liste_joueurs_restants[cls.joueur_actuel]
            cls.affJoueursRestants()
            print('\n')
            input(j)
            if pre_flop:
                print('cartes : {}'.format(j.cartes))
            else:
                print('cartes : {} , \nmilieu : {}'.format(j.cartes , cls.milieu))
                j.combiMain()
                print(msgMain(j.combi))
            print('pot: {}'.format(cls.pot))
            j.miser()
            if cls.joueur_actuel == cls.nb_joueurs_restants:
                cls.joueur_actuel = 0
            cls.tousAlignes()
    tour = classmethod(tour)

    def entreTours(cls , flop=False):
        '''
        actions entre 2 tours d'enchere
        '''
        cls.liste_joueurs_temp = cls.liste_joueurs_restants[:]
        cls.nb_joueurs_temp = cls.nb_joueurs_restants
        if flop:
            cls.ajouterCartes(3)
        else:
            cls.ajouterCartes()
        if cls.nb_joueurs_restants > 1:
            cls.quiCommence()
        for j in cls.liste_joueurs_temp:
            j.mise = 0
            j.a_joue = False
        cls.aligne = False
        cls.mise_max = 0
    entreTours = classmethod(entreTours)

    def showDown(cls):
        '''
        Révélation des cartes, le meilleur gagne
        '''
        if cls.nb_joueurs_restants == 1:
            cls.liste_joueurs_restants[0].gagner()
        else:
            g = 0
            for j in range(cls.nb_joueurs_restants):
                print('{} : {}'.format(cls.liste_joueurs_restants[j].nom , cls.liste_joueurs_restants[j].cartes))
                if comparMain(cls.liste_joueurs_restants[j].combi , cls.liste_joueurs_restants[g].combi) == 1:
                    g = j
            print('milieu : {}'.format(cls.milieu))
            cls.liste_joueurs_restants[g].gagner()
    showDown = classmethod(showDown)
        
    def jouer(cls):
        '''
        enchainer les parties
        '''
        cls.initJeu()
        while cls.nb_joueurs > 1:
            cls.initPot()
            cls.distrib()
            cls.placerBlindes()
            for j in cls.liste_joueurs:
                if j.est_utg:
                    cls.joueur_actuel = cls.liste_joueurs.index(j)
            for k in range(4):
                if k == 0:
                    cls.tour(pre_flop=True)
                    cls.entreTours(flop=True)
                else:
                    cls.tour()
                if k != 3 and k != 0:
                    cls.entreTours()
            cls.showDown()
            cls.sousous()
            time.sleep(5)
            print('\n' * 50)
    jouer = classmethod(jouer)

Joueur.jouer()