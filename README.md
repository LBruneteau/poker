# Texas Hold'em Python

Ce programme vous permet de jouer au *poker Texas Hold'em*  
Il existe deux versions :  
**Texas_Holdem.py**, qui se joue dans un **terminal** et n'a besoin d'aucun module  
**Texas_Holdem_GUI.py**, qui dispose d'une **interface graphique**  

## Comment installer le programme ?

Téléchargez les fichiers et décompressez-les  
Si vous voulez une interface graphique, vous aurez besoin des modules easygui , pillow , playsound et PyObjC  
`pip install easygui pillow playsound PyObjC`  
Le fichier *module_poker.py* doit forcément être dans le même dossier que les autres fichiers  

## Comment lancer le programme ?

Si vous voulez utiliser la version textuelle il faut éxécuter *Texas_Holdem.py* `python3 /chemin/vers/Texas_Holdem.py`  
Si vous voulez une interface graphique il faut lancer *Texas_Holdem_GUI.py* `python3 /chemin/vers/Texas_Holdem_GUI.py`  

## Comment jouer ?

Si vous voulez apprendre à utiliser la version graphique, lancez-la et cliquez sur `guide du programme`  
Ce guide sera donc exclusif à la **version textuelle**  

#### Commencer une partie

Tout d'abord entrez le **nombre de joueurs**. Le minimum est 2 et le maximum 23  
Entrez ensuite les **noms des joueurs** un par un.  

#### Les joueurs

Les joueurs sont d'abord représntés par leur **roles** (*Dealer* ,*Petite Blinde* , *Grosse Blinde* , *Under The Gun*)  
Ils ont ensuite leur **nom** d'indiqué  
Ensuite leur **argent** est écrit entre crochets  
Puis leur **mise** est écrite  
Une **flèche** suit le nom du joueur s'il joue actuellement  

###### Exemple  

`D              Paul         [1350] : 35 <-`  
Ici **Paul** est **Dealer**, possède **1350** et a misé **35**. C'est à *son tour*

#### Les cartes  

Les cartes sont un couple (symbole , valeur)  
Un *J* correspond à un **valet**  
Un *Q* correspond à une **dame**    
Un *K* correspond à un **roi**  
Un *A* correspond à un **As**  

#### Les tours  

Au début d'un tour tous les joueurs sont affichés et l'un d'entre eux joue.  
Il doit appuyer sur **Entrée** pour commencer son tour  
`Les autres joueurs ne regardent plus !!`  
Ici le joueur **voit ses cartes**.  
Il doit *miser*  
Il y a une mise minimale pour **s'aligner**.  
Si le joueur mise moins il **se couche**  
Si il mise plus il **relance**  
Puis c'est le tour du *joueur suivant*

###### remerciements

Merci à *Bastien* et à *Elouan* de m'avoir aidé à tester le programme  
Merci à *Ethan*, qui a cru en moi jusqu'au bout