from module_poker import jeuCartes , pioche , comparMain , msgMain
import easygui as eg

def jouerSon(son:str):
    '''
    Essaie de jouer un son , de chemin donné en str
    Si une erreur se produit, rien ne se passe
    '''
    #Si une erreur se produit dans le bloc try, le programme ne plante pas mais execute le bloc except
    try:
        import playsound #Module pour jouer du son
        playsound.playsound(son)
    except: #Si une erreur se produit
        pass #On ne fait rien

def tuto():
    '''
    Apprend à l'utilisateur à se servir de l'app
    '''
    t:str = 'Texas Hold\'em'
    ok:str = 'suivant'
    eg.msgbox(msg='Bienvenue dans le guide de l\'utilisateur\nVous allez apprendre à vous servir de ce programme', title=t , ok_button=ok)
    eg.msgbox(msg='Vous devrez tout d\'abord entrer le nombre de joueurs.\nLe minimum est 2 et le maximum 23', title=t, ok_button=ok, image='menu.png')
    eg.msgbox(msg='Il faudra ensuite entrer les noms des joueurs un par un', title=t, ok_button=ok, image='entrer_nom.png')
    eg.msgbox(msg='Un pot peut commencer\nLes blindes sont à 10 et 20', title=t, ok_button=ok, image='au_tour.png')
    eg.msgbox(msg='Cet écran sera affiché pendant le tour des joueurs.\nEn haut sont représentés les différents joueurs\nTout d\'abord son ou ses rôles sont affichés, s\'il en a.\nEnsuite son nom est écrit\nLe nombre entre crochets est l\'argent du joueur\nEnsuite sa mise est affichée\nUne flèche (<-) signifie que le joueur joue actuellement\n', title=t , ok_button=ok , image='repr_joueurs.png')
    eg.msgbox(msg='Le pot est l\'argent total misé par les joueurs pendant la partie\nS\'il y a des cartes au milieu, elles seront affichées\nUne cartes est un couple (symbole:str , valeur:str)\nJ -> Valet = 11\nQ -> Dame = 12\nK -> Roi = 13 \nA -> As = 14', title=t , ok_button=ok , image='cartes_communes.png')
    eg.msgbox(msg='Le bouton "voir mes cartes" affiche cet écran .\nIci les cartes du joueur sont représentées comme précédemment. \nLes joueurs non concernés ne doivent pas regarder\nS\'il y a des cartes communes elles seront également affichées, ainsi que la combinaison du joueur\nLe bouton "retour" renvoie sur l\'écran précédent', title=t , ok_button=ok ,image='ecran_cartes.png')
    eg.msgbox(msg='L\'option "suivre" permet de s\'aligner rapidement à la somme la plus élevée', title=t , ok_button=ok , image='repr_joueurs.png')
    eg.msgbox(msg='Le bouton "se coucher" fait sortir le joueur du pot\nIl jouera le suivant' , title=t , ok_button=ok , image='repr_joueurs.png')
    eg.msgbox(msg='Si vous cliquez sur "relancer", cet écran apparaitra\nIl vous permettra d\'augmenter la mise max', title=t , ok_button=ok , image='ecran_raise.png')
    eg.msgbox(msg='Enfin, si tous les joueurs sont alignés, vous pouvez checker avec le bouton correspondant', title=t , ok_button=ok , image='ecran_check.png')
    eg.msgbox(msg='Une fois que le joueur a fait une action, un écran s\'affiche', title=t , ok_button=ok, image='check.png')
    eg.msgbox(msg='Un nouveau tour commence' , title=t , ok_button=ok , image='au_tour.png')
    eg.msgbox(msg='Si tous les joueurs se couchent, le dernier en jeu remporte le pot', title=t , ok_button=ok , image='gagner.png')
    eg.msgbox(msg='Sinon, les cartes de chacun sont révélées et le pot est accordé au joueur avec la meilleure main', title=t ,image='showdon.png', ok_button='suivant')
    eg.msgbox(msg='Si un joueur n\'a plus d\'argent, il est éliminé', title=t, ok_button=ok)

class Joueur:
    
    '''
    Classe des joueurs de poker
    Contient toutes les fonctions nécessaires à une partie
    Ne concerne pas les cartes, gérées par module_poker
    '''

    nb_joueurs:int = 0 #nombre de joueurs total
    nb_joueurs_temp:int = 0 #nombre de joueurs au début du tour
    nb_joueurs_restants:int = 0 #nombre de joueurs encore dans la partie
    liste_joueurs:list = [] #Liste de Joueur
    liste_joueurs_temp:list = [] #liste des joueurs au début du tour
    liste_joueurs_restants:list = [] #les joueurs qui ne sont pas couchés
    pot:int = 0 #total misé
    jeu:list = jeuCartes()
    joueur_actuel:int = 0 #index du joueur dans la liste des joueurs restants
    mise_max:int = 0 #Plus haute mise, pour s'aligner
    milieu:list = [] #cartes communes

    def __init__(self,nom:str):
        '''
        création d'un objet de type Joueur en fonction de son nom (str)
        A des cartes de l'argent etc...
        Peut checker , se coucher etc...
        '''
        self.nom:str = nom
        self.argent:str = 1500
        self.mise:int = 0 #L'argent misé par le joueur pendant le tour. Référence pour s'aligner
        self.cartes:list = []
        self.a_joue:bool = False #Si son tour est passé
        self.est_tapis:bool = False #Si le joueur est à tapis
        self.est_dealer:bool = False
        self.est_petite_blinde:bool = False
        self.est_grosse_blinde:bool = False
        self.est_utg:bool = False #Under The Gun -> après la BB, premier à jouer préflop
        self.combi:list = [] #Les 5 meilleures cartes parmi la ùain et le milieu
        Joueur.nb_joueurs += 1
        Joueur.liste_joueurs.append(self)

    def __repr__(self):
        '''
        permet d'obtenir le joueur en temps que str
        D'abord le role (D, SB, BB, UTG ) puis le nom l'argent et la mise, une fleche si c'est le joueur qui joue
        '''
        msg = ''
        if self.est_dealer:
            msg += 'D '
        if self.est_petite_blinde:
            msg += 'SB '
        if self.est_grosse_blinde:
            msg += 'BB '
        if self.est_utg:
            msg += 'UTG '
        msg += ' ' * (8 - len(msg))
        msg += self.nom.center(20 , ' ')
        msg += ' [{}] : {}'.format(str(self.argent).center(6,' ') , str(self.mise).center(5 , ' '))
        if Joueur.liste_joueurs_restants[Joueur.joueur_actuel] is self:
            msg += " <- "
        return msg

    def affJoueurs(cls):
        '''
        retourne tous les joueurs sous forme de str
        1 par ligne
        '''
        msg:str = ''
        for j in cls.liste_joueurs:
            msg += str(j) + '\n'
        return msg
    affJoueurs = classmethod(affJoueurs)

    def affJoueursRestants(cls):
        ''''
        retourne tous les joueurs restants sous forme de str
        1 par ligne
        '''
        msg:str = ''
        for j in cls.liste_joueurs_restants:
            msg += str(j) + '\n'
        return msg
    affJoueursRestants = classmethod(affJoueursRestants)

    def initJeu(cls):
        '''
        permet de créer une partie (nb de joueurs, leurs noms ,les roles...)
        '''
        import random
        liste_noms:tuple = ('Gisemonde', 'Nathanael', 'Marjolaine', 'Gudule', 'Gustave', 'Hermeline', 'Donatien', 'Marceau', 'Pierrick', 'Cunégone', 'Rudolf')
        cb_joueurs:int = eg.integerbox(msg='Bienvenue à la table !\nÀ combien désirez-vous jouer ?' , title='Texas Hold\'em'  , lowerbound=2 , upperbound=23)
        for j in range(cb_joueurs):
            nom:str = eg.enterbox(msg='Nom du joueur {} : '.format(j + 1), title='Texas Hold\'em', default=random.choice(liste_noms))
            Joueur(nom)
        cls.nb_joueurs_restants = cls.nb_joueurs 
        cls.liste_joueurs_restants = cls.liste_joueurs[:]
        cls.nb_joueurs_temp = cls.nb_joueurs
        cls.liste_joueurs_temp = cls.liste_joueurs[:]
        #roles
        cls.liste_joueurs[-1].est_dealer = True          
    initJeu = classmethod(initJeu)

    def tournerDealer(cls):
        for j in cls.liste_joueurs:
            i = cls.liste_joueurs.index(j)
            if j.est_dealer:
                if i == cls.nb_joueurs - 1:
                    cls.liste_joueurs[0].est_dealer = True
                else:
                    cls.liste_joueurs[i + 1].est_dealer = True
                j.est_dealer = False
                break
    tournerDealer = classmethod(tournerDealer)

    def initPot(cls):
        '''
        permet de commencer un pot. 
        remélange les cartes, remet tous les joueurs en jeu, fait tourner les roles...
        '''
        cls.nb_joueurs_restants = cls.nb_joueurs #Retour des joueurs couchés dans la partie
        cls.liste_joueurs_restants = cls.liste_joueurs[:]
        cls.jeu = jeuCartes() #Jeu complet
        cls.pot = 0
        cls.joueur_actuel = 0
        cls.mise_max = 0
        cls.milieu =[] #Plus de cartes au milieu
        cls.nb_joueurs_temp = cls.nb_joueurs
        cls.liste_joueurs_temp = cls.liste_joueurs[:]
        cls.tournerDealer()
        for j in cls.liste_joueurs:
            #Réinitialiser certains attributs des joueurs
            j.est_grosse_blinde = False
            j.est_petite_blinde = False
            j.est_utg = False
            j.mise = 0
            j.cartes = []
            j.a_joue = False
            j.est_tapis = False
        for j in cls.liste_joueurs: #2 boucles nécessaires
            i = cls.liste_joueurs.index(j)
            #L'ordre est D SB BB UTG
            #On retrouve le D et on applique aux joueurs suivants
            if j.est_dealer:
                #               SB    BB     UTG
                index:tuple = (i + 1, i + 2, i + 3)
                l:list = []
                for pos in index:
                    while pos >= cls.nb_joueurs:
                        pos -= cls.nb_joueurs
                    l.append(pos)
                cls.liste_joueurs[l[0]].est_petite_blinde = True
                cls.liste_joueurs[l[1]].est_grosse_blinde = True
                cls.liste_joueurs[l[2]].est_utg = True
    initPot = classmethod(initPot)

    def distrib(cls):
        '''
        2 cartes pour chaque joueur
        '''
        for j in cls.liste_joueurs: #pour chaque joueur
            cls.jeu , cartes = pioche(cls.jeu , 2) #Fonction pioche dans module_poker.py
            j.cartes = cartes #Donner les cartes au joueur
    distrib = classmethod(distrib)

    def fold(self):
        '''
        permet de se coucher.
        Retire le joueur de la liste des joueurs restants et affiche un message
        '''
        eg.msgbox(msg='{} se couche'.format(self.nom) , title='Texas Hold\'em' , ok_button='joueur suivant')
        Joueur.liste_joueurs_restants.remove(self) #On retire le joueur des joueurs restants
        Joueur.nb_joueurs_restants -= 1 #-1 au nombre de joueurs restants
        #Joueur.joueur_actuel n'augmente pas car retirer un joueur passe déjà au suivant
        self.a_joue = True #Le joueur a joué pendant le tour

    def check(self):
        '''
        Fonction por checker (miser 0)
        '''
        eg.msgbox(msg='{} check'.format(self.nom) , title='Texas Hold\'em'  , ok_button='joueur suivant')
        self.a_joue = True #On a joué
        Joueur.joueur_actuel += 1 #On passe la parole au joueur suivant (index dans les joueurs restants)

    def bet(self , mise:int , tour_normal=True):
        '''
        permet de miser une certaine somme d'argent
        gere le tapis
        relance ou suit en fonction de la mise
        tour normal = False quand on place les blindes pour ne pas avoir de msgbox
        '''
        if mise >= self.argent: #Si on part à tapis
            if tour_normal: #Afficher un message si on ne place pas de blinde
                eg.msgbox(msg='{} mise {} et est à tapis !'.format(self.nom , self.argent) , title='Texas Hold\'em'  , ok_button='joueur suivant')
            self.mise += self.argent #On augmente notre mise
            Joueur.pot += self.argent #On ajoute l'argent dans le pot
            self.argent = 0 #Le joueur n'a plus d'argent (tapis)
            self.est_tapis = True 
            Joueur.mise_max = max(Joueur.mise_max , self.mise) #mettre à jour la mise max
        else:
            self.mise += mise 
            Joueur.pot += mise
            self.argent -= mise
            #Si on suit
            if self.mise == Joueur.mise_max: 
                if tour_normal:
                    eg.msgbox(msg='{} suit {}'.format(self.nom , mise) , title='Texas Hold\'em'  , ok_button='joueur suivant')
            else:
            #Si on relance
                if tour_normal:
                    eg.msgbox(msg='{} relance à {}'.format(self.nom , self.mise) , title='Texas Hold\'em'  , ok_button='joueur suivant')
                Joueur.mise_max = self.mise
        Joueur.joueur_actuel += 1 #On passe le tour au suivant
        self.a_joue = True

    def miser(self , mini=20):
        '''
        utilisé pour relancer
        le mini est le double de la mise max moins la mise du joueur
        '''
        mess:str = 'Vous avez {}\nLa mise maximale est {}\nVous devez rajouter au moins {}\nCombien misez-vous ?'.format(self.argent , Joueur.mise_max , mini)
        mise:int = eg.integerbox(msg=mess , title='Texas Hold\'em' , lowerbound=min(mini , self.argent) , upperbound=99999999999999999999999999999999999999999, default=mini)
        if mise == None: #Si la réponse est vide ou si le joueur fait cancel
            mise = mini
        self.bet(mise)

    def tousAlignes(cls):
        '''
        vérifie si tous les joueurs ont joués et sont alignés
        permet de passer au tour suivant
        '''
        #S'il n'y a plus qu'un joueur ils sont alignés
        if cls.nb_joueurs_restants == 1:
            aligne:bool = True
        else:
            #On vérifie si tout le monde a joué
            tous_joue = True
            j = 0
            while tous_joue and j < cls.nb_joueurs_temp:
                tous_joue = cls.liste_joueurs_temp[j].a_joue or cls.liste_joueurs_temp[j].est_tapis
                j += 1
            aligne = tous_joue
            j = 0
            #On vérifie qu'ils sont tous à la mise max
            while aligne and j < cls.nb_joueurs_restants:
                aligne = cls.liste_joueurs_restants[j].mise == cls.mise_max or cls.liste_joueurs_temp[j].est_tapis
                j += 1
        return aligne
    tousAlignes = classmethod(tousAlignes)

    def sousous(cls):
        '''
        Retire des joueurs ceux qui ont tout perdu
        fait tourner les blindes si le joueur perdant est D
        '''
        for j in cls.liste_joueurs: #On va vérifier pour tous les joueurs
            if j.argent == 0: #Ils ont perdu si leur argent est à 0
                if j.est_dealer:
                    Joueur.tournerDealer()
                cls.liste_joueurs.remove(j)
                cls.nb_joueurs -= 1
    sousous = classmethod(sousous)

    def placerBlindes(cls):
        '''
        Les joueur qui sont petite et grosse blinde placent respectivement 10 et 20
        '''
        #2 boucles séparées pour que la petite blinde soit placée avant la grosse
        #False dans bet permet de ne pas avoir de message
        for j in cls.liste_joueurs:
            if j.est_petite_blinde:
                j.bet(10 , False)
                j.a_joue = False
        for j in cls.liste_joueurs:
            if j.est_grosse_blinde:
                j.bet(20 , False)
                j.a_joue = False
    placerBlindes = classmethod(placerBlindes)

    def ajouterCartes(cls,nb_cartes=1):
        '''
        ajoute des cartes au milieu
        '''
        cls.jeu , carte_piochees = pioche(cls.jeu , nb_cartes)
        cls.milieu += carte_piochees
    ajouterCartes = classmethod(ajouterCartes)

    def combiMain(self):
        '''
        Donne à un joueur la meilleure main parmi le milieu et ses cartes
        '''
        from itertools import combinations #Pour tester toutes les combinaisons possibles
        cartes_dispo:list = Joueur.milieu[:] + self.cartes[:] #Total des cartes pour faire des combis
        combis:tuple = tuple(combinations(cartes_dispo , 5)) #Toutes les combis de 5 cartes avec celles du milieu et les cartes de la main
        #On cherche la meilleure combinaison
        maxi:tuple = combis[0]
        for c in combis:
            if comparMain(c , maxi) == 1: #Si la main c est meilleure que la maxi
                maxi = c
        self.combi = maxi

    def gagner(self):
        '''
        donne à un joueur l'argent du pot
        Ne vérifie pas qui gagne
        '''
        self.argent += Joueur.pot
        eg.msgbox(msg='{} remporte un pot de {}'.format(self.nom , Joueur.pot) , title='Texas Hold\'em' , ok_button='pot suivant')
        Joueur.pot = 0
        
    def quiCommence(cls):
        '''
        determine le joueur qui commence un tour.
        D'abord la SB, puis les autres joueurs dans l'ordre s'ils sont couchés
        '''
        #recherche de la SB
        for j in cls.liste_joueurs:
            if j.est_petite_blinde:
                i = cls.liste_joueurs.index(j)  
        petite_blinde_restante = cls.liste_joueurs[i] in cls.liste_joueurs_restants
        #Si elle est couchée
        if not petite_blinde_restante:
            #recherche du joueur suivant
            #On vérifie pour les joueurs dans l'ordre s'ils sont couchés
            k = 0
            trouve = False
            while not trouve:
                if i + k >= cls.nb_joueurs_restants:
                    k -= cls.nb_joueurs_restants
                if cls.liste_joueurs[i + k] in cls.liste_joueurs_restants:
                    trouve = True
                    cls.joueur_actuel = cls.liste_joueurs_restants.index(cls.liste_joueurs[i + k])
                k += 1
        else:
            cls.joueur_actuel = i
    quiCommence = classmethod(quiCommence)

    def ecranBase(self, au_tour=True):
        '''
        gere la GUI au tour du joueur
        propose des actions adaptées
        '''
        if au_tour: #Si c'est le début du tour et pas un retour en arrirere
            eg.msgbox(msg='Au tour de ' + self.nom , title='Texas Hold\'em', ok_button='jouer')
        a_suivre:int = Joueur.mise_max - self.mise #L'argent que le joueur doit payer pour suivre
        mess:str = '{} \npot : {} \n'.format(Joueur.affJoueursRestants() , Joueur.pot) #Ce qu'il faut afficher (les joueurs, le milieu, le pot...)
        if Joueur.milieu != []:
            mess += 'milieu : {}'.format(Joueur.milieu) #ajouter les cartes au milieu s'il y en a 
        if a_suivre == 0: #Si on peut checker et on le propose on retire l'option se coucher 
            choix = eg.buttonbox(msg=mess , title='Texas Hold\'em' , choices=('voir mes cartes' , 'relancer' , 'check') ,default_choice='check')
            if choix == 'voir mes cartes':
                #Aller sur le menu des cartes
                message_cartes = 'cartes : {} \n'.format(self.cartes) #Contient les cartes du joueur 
                if Joueur.milieu != []: #Mettre les cartes communes et le nom de la meilleure combi
                    message_cartes += 'milieu : {}\n{}'.format(Joueur.milieu, msgMain(self.combi))
                eg.msgbox(msg=message_cartes , title='Texas Hold\'em' , ok_button='retour')
                self.ecranBase(False) #Ensuite retourner sur le menu des choix
            elif choix == 'check':
                self.check()
            else:
                self.miser(20) #Demander au joueur de miser au moins 20
        else: #Si on ne peut pas checker
            #Les options sont se coucher, suivre, voir les cartes et relancer
            choix = eg.buttonbox(msg=mess , title='Texas Hold\'em' , choices=('voir mes cartes' , 'suivre {}'.format(a_suivre) , 'se coucher' , 'relancer') ,default_choice='suivre {}'.format(a_suivre))
            if choix == 'voir mes cartes': 
                message_cartes = 'cartes : {} \n'.format(self.cartes)
                if Joueur.milieu != []:
                    message_cartes += 'milieu : {}\n{}'.format(Joueur.milieu , msgMain(self.combi))
                eg.msgbox(msg=message_cartes , title='Texas Hold\'em' , ok_button='retour')
                self.ecranBase(False)
            elif choix == 'suivre {}'.format(a_suivre):
                self.bet(a_suivre) #Miser le nécessaire pour suivre
            elif choix == 'se coucher':
                self.fold()
            elif choix == 'relancer':
                self.miser(Joueur.mise_max * 2 - self.mise) #Relancer au moins au double de la mise max

    def tour(cls , pre_flop=False):
        '''
        fait jouer chacun des joueurs jusq'a ce qu'ils soient alignés
        '''
        while not cls.tousAlignes(): #Répéter tant que les joueurs n'ont pas joués / ne sont pas couchés / relancent
            if cls.joueur_actuel == cls.nb_joueurs_restants:
                cls.joueur_actuel = 0
            j = cls.liste_joueurs_restants[cls.joueur_actuel] #Obtenir le joueur qui joue
            #preflop permet de savoir s'il y a des cartes communes 
            if not j.est_tapis:
                if not pre_flop:
                    j.combiMain() #Obtenir notre main
                j.ecranBase() #Faire apparaitre l'interface
            else:
                j.joueur_actuel += 1
                #Joueur actuel est un index de la liste des joueurs restants qui augmente
                #doit parfois retourner à 0 pour éviter l'IndexError
            if cls.joueur_actuel == cls.nb_joueurs_restants:
                cls.joueur_actuel = 0
    tour = classmethod(tour)

    def entreTours(cls , flop=False):
        '''
        prépare le tour suivant (remet les mises à 0, ajoute un milieu...)
        '''
        cls.liste_joueurs_temp = cls.liste_joueurs_restants[:] #Utiliser liste joueurs temp comme référence pou savoir qui a joue
        cls.nb_joueurs_temp = cls.nb_joueurs_restants
        if flop: #Au flop on ajoute 3 cartes au milieu au lieu d'une
            cls.ajouterCartes(3)
        else:
            cls.ajouterCartes()
        if cls.nb_joueurs_restants > 1: #On ne vérifie qui commence que s'il reste plusieurs joueurs
            cls.quiCommence() #joueur actuel est celui qui va jouer en premier
        for j in cls.liste_joueurs_temp:
            j.mise = 0
            j.a_joue = False
        cls.mise_max = 0
    entreTours = classmethod(entreTours)

    def showDown(cls):
        '''
        Fin de partie -> révélation des cartes et gain du pot pour la meilleure combinaison
        ne prend pas encore en compte l'égalité 
        '''
        #S'il n'y a plus qu'un joueur le faire gagner
        if cls.nb_joueurs_restants == 1:
            cls.liste_joueurs_restants[0].gagner()
        else:
            #Sinon monter les cartes de tout le monde et donner le pot au meilleur
            mess:str = ''
            liste_gagnant:list = [0] #Index dans la liste des joueurs restants Liste pour gerer les égalités
            for j in range(cls.nb_joueurs_restants):
                mess += '{} : {} : {} \n'.format(cls.liste_joueurs_restants[j].nom , cls.liste_joueurs_restants[j].cartes , msgMain(cls.liste_joueurs_restants[j].combi))
                if comparMain(cls.liste_joueurs_restants[j].combi , cls.liste_joueurs_restants[liste_gagnant[0]].combi) == 1:
                    liste_gagnant = [j]
                elif comparMain(cls.liste_joueurs_restants[j].combi , cls.liste_joueurs_restants[liste_gagnant[0]].combi) == 0 and j not in liste_gagnant:
                    liste_gagnant.append(j)
            mess += 'milieu : {}'.format(cls.milieu)
            eg.msgbox(msg=mess, title='Texas Hold\'em', ok_button='suivant')
            if len(liste_gagnant) == 1:
                cls.liste_joueurs_restants[liste_gagnant[0]].gagner()
            else:
                eg.msgbox(msg='Partage du pot', title='Texas Hold\'em', ok_button='pot suivant')
                part = cls.pot // len(liste_gagnant)
                for j in liste_gagnant:
                    if j == liste_gagnant[-1]:
                        cls.liste_joueurs_restants[liste_gagnant[j]].argent += cls.pot
                    else:
                        cls.liste_joueurs_restants[liste_gagnant[j]].argent += part
                        cls.pot -= part
                cls.pot = 0
    showDown = classmethod(showDown)
        
    def jouer(cls):
        '''
        Propose de suivre un tutoriel de poker, d'apprendre à utiliser le programme ou de jouer
        Gère l'ensemble des parties faites
        Est actif du début à la fin
        '''
        import webbrowser
        options:tuple = ('Jouer' ,'Regles du jeu', 'Guide du programme')
        choix:str = eg.buttonbox(msg='Bienvenue, que désirez-vous faire ?', title='Texas Hold\'em', choices=options , default_choice='Jouer')
        if choix == 'Regles du jeu':
            eg.msgbox(msg='Cette video va vous apprendre à jouer au poker', title='Texas Hold\'em', ok_button='retour')
            webbrowser.open_new('https://www.youtube.com/watch?v=MaMoyipuGu4')
            cls.jouer()
        elif choix == 'Guide du programme':
            tuto()
            cls.jouer()
        cls.initJeu()
        while cls.nb_joueurs > 1: #Continuer de faire des parties tant que tout le monde n'a pas perdu
            cls.initPot()
            cls.distrib()
            cls.placerBlindes()
            for j in cls.liste_joueurs:
                if j.est_utg: #Faire jouer en premier le UTG
                    cls.joueur_actuel = cls.liste_joueurs.index(j)
            for k in range(4): #répéter pour les 4 tours
                if k == 0: #Au premier tour on agit differement
                    cls.tour(pre_flop=True)
                    cls.entreTours(flop=True)
                else:
                    cls.tour()
                if k != 3 and k != 0: #Pas d'entre tour avant l'abattage
                    cls.entreTours()
            cls.showDown()
            cls.sousous()
        eg.msgbox(msg='La partie est terminée !\nMerci d\'avoir joué !', title='Texas Hold\'em', ok_button='fermer')
    jouer = classmethod(jouer)

Joueur.jouer() #Lancement du jeu